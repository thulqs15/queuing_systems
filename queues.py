import numpy as np
import random
from tools import ordered_BVN, phi1

class Queue:
    def __init__(self, lbd, K, strategy=None):
        self.lbd=lbd
        self.packets = []
        self.chosen_server = None
        self.server_history = np.zeros(K)
        self.success =None
        self.strategy = strategy

    def receive_packet(self,t):
        if np.random.binomial(1,self.lbd)==1:
            self.packets.append(t)

    def choose_packet(self):
        return self.packets[0]

class collab(Queue):

    def __init__(self,lbd,K,N,rank,C=None,arrival_rates=None,mu=None,test=False,alpha=1/5):
        super().__init__(lbd, K, "collab")
        self.rank = rank
        self.K = K
        self.N = N
        self.C = C

        self.test=test
        if self.test:
            self.lambda_estimates = arrival_rates
            self.mu_estimates = mu
            self.P = phi1(self.lambda_estimates,self.mu_estimates,self.N,self.P)
        else:
            self.lambda_estimates = np.zeros(self.K)
            self.lambda_estimates[rank] =  self.lbd
            self.mu_estimates = np.zeros(self.K)
            self.P = np.ones((N,K))/N

        self.coef, self.perm = ordered_BVN(self.P,self.C)
        self.exploration_parameters = {"explore":False,"w2":0,"n":0,"j":0,"update":0}
        self.counter = {"pulls_servers":np.zeros(self.K),"pulls_queues":np.zeros(self.N),"S":np.zeros(self.N),"success_queues":np.zeros(self.N),"success_servers":np.zeros(self.K)}
        self.alpha = alpha

    def choose_server(self):
        seed = random.random()
        a = self.coef[0]
        i=0
        while seed>a:
            i+=1
            a+=self.coef[i]
        return self.perm[i][self.rank]

    def play(self, K,t):
        if len(self.packets) == 0:
            return -1,-1

        a = random.random()
        if a<(self.K+self.N)/(t+1)**(self.alpha) and not self.test:
            self.trigger_exploration(t)
            packet, server = self.explore(t)
        else:
            self.exploration_parameters["explore"]=False
            if self.exploration_parameters["update"] >100:
                self.P = phi1(self.lambda_estimates,self.mu_estimates,self.N,self.P)
                self.coef, self.perm = ordered_BVN(self.P,self.C)
                self.exploration_parameters["update"] = 0
            packet = self.choose_packet()
            server = self.choose_server()

        self.choosen_server = server
        self.server_history[server]+=1
        self.success = False
        return t-packet, server

    def cleared(self):
        self.success = True
        if self.exploration_parameters["explore"] and self.exploration_parameters["type"]=="queue":
            self.packets.pop(-1)
        else:   
            self.packets.pop(0)

    def update_state(self):
        if self.exploration_parameters["explore"]==False:
            return
        if self.exploration_parameters["type"]=="server":
            k = self.exploration_parameters["server"]
            self.counter["success_servers"][k]+=self.success
            self.counter["pulls_servers"][k]+=1
            self.mu_estimates[k] = self.counter["success_servers"][k]/self.counter["pulls_servers"][k]
        elif not self.exploration_parameters["queue"]==-1:
            j = self.exploration_parameters["queue"]
            self.counter["success_queues"][j]+=self.success
            self.counter["pulls_queues"][j]+=1
            self.counter["S"][j] = self.counter["success_queues"][j]/self.counter["pulls_queues"][j]
            if  np.dot(self.mu_estimates,self.counter["pulls_servers"])>0:
                hat_mu = np.dot(self.mu_estimates,self.counter["pulls_servers"])/np.sum(self.counter["pulls_servers"])
                self.lambda_estimates[j] = min(1,max(0,2*(1-self.counter["S"][j]/hat_mu)))
        return

    def trigger_exploration(self,t):
        self.exploration_parameters["explore"] = True
        self.exploration_parameters["n"] = random.choice(range(self.N+self.K))
        self.exploration_parameters["type"] = None
        self.exploration_parameters["queue"] = -1
        self.exploration_parameters["server"] = -1
        self.exploration_parameters["update"] += 1
        #print("time: ",t, " rank:",self.rank,"n:",self.exploration_parameters["n"])

    def explore(self,t):
        if self.exploration_parameters["n"]<self.K:
            self.exploration_parameters["type"] = "server"
            return self.choose_explored_server(t)
        elif self.packets[-1]==t:
            self.exploration_parameters["type"] = "queue"
            return self.queue_exploration(t)
        else:
            return t+1,-1

    def choose_explored_server(self,t):
        server = (self.exploration_parameters["n"]+self.rank)%self.K
        packet = self.packets[0]
        self.exploration_parameters["server"] = server
        self.choosen_server = server
        self.server_history[server]+=1
        self.success = False
        return packet, server

    def queue_exploration(self,t):
        n_rounds = 2*int((self.N+1)/2)-1
        r = random.choice(np.array(range(n_rounds)))
        self.exploration_parameters["queue"] = self.choose_explored_queue(r)
        server = self.choose_common_server()
        self.choosen_server = server
        self.server_history[server]+=1
        self.success = False
        return t, server

    def choose_explored_queue(self,r):
        N = 2*int((self.N+1)/2)
        if self.rank==0:
            j = r+1
        elif self.rank==r+1:
            j = 0
        else:
            j =(self.rank+2*(N-self.rank+r))%(N-1)
            if j==0:
                j = N-1
        if j==self.N:
            j=-1
        return j

    def choose_common_server(self):
        l = random.choice(np.array(range(self.K)))
        if self.exploration_parameters["queue"]==-1:
            k = -1
        else:
            k = (l+max(self.rank,self.exploration_parameters["queue"]))%self.K
        return k



class EXP3P(Queue):
    """
    EXP3.P from "The Nonstochastic Multiarmed Bandit Problem"
    """
    def __init__(self, lbd, K, T=None, delta=0.5):
        super().__init__(lbd, K, "EXP3.P")
        self.weights = np.ones(K)/K
        self.K = K
        self.T = T
        self.gamma = min(3/2, 2*np.sqrt(3*K*np.log(K)/(5*T)))
        self.alpha = 2*np.sqrt(np.log(K*T/delta))        
        self.estimates = np.ones(K)*np.exp(self.alpha*self.gamma*np.sqrt(T/K)/3)

    def choose_server(self):
        s = np.random.uniform()
        l = self.weights[0]
        for i in range(self.K):
            if s<l:
                return i
            else:
                l+=self.weights[i+1]
        return random.randint(0, self.K-1)
        
    def update_state(self):
        self.estimates *= np.exp(self.gamma*self.alpha/(3*self.K*self.weights*np.sqrt(self.K*self.T)))
        self.estimates[self.choosen_server] *= np.exp(self.gamma*self.success/(3*self.K*self.weights[self.choosen_server]))
        self.weights =(1-self.gamma)*self.estimates/np.sum(self.estimates)+self.gamma/self.K


    def play(self, K,t):
        if len(self.packets) == 0:
            return -1,-1
        packet = self.choose_packet()
        server = self.choose_server()
        self.choosen_server = server
        self.server_history[server]+=1
        self.success = False
        return t-packet, server


    def cleared(self):
        self.success = True
        self.packets.pop(0)

class EXP3P1(EXP3P):

    def __init__(self, lbd, K, delta=0.5):
        self.delta = delta
        self.K = K
        self.init_r()
        super().__init__(lbd, K, T=2**self.r, delta=self.delta/((self.r+1)*(self.r+2)))
        self.strategy = "EXP3.P.1"
        self.t = 0
        
    def init_r(self):
        f = lambda n: self.K/self.delta*2**n*np.exp(-self.K*2**n)*(n+1)*(n+2)
        self.r = 1
        while f(self.r)>1:
            self.r+=1

    def update_state(self):
        super().update_state()
        self.t += 1
        if self.t == 2**self.r:
            # reset EXP3.P
            self.r += 1
            self.t = 0
            self.T *= 2
            self.weights = np.ones(self.K)/self.K
            self.gamma = min(3/2, 2*np.sqrt(3*self.K*np.log(self.K)/(5*self.T)))
            self.alpha = 2*np.sqrt(np.log(self.K*self.T*(self.r+1)*(self.r+2)/self.delta))        
            self.estimates = np.ones(self.K)*np.exp(self.alpha*self.gamma*np.sqrt(self.T/self.K)/3)
import numpy as np
from scipy.optimize import linear_sum_assignment
#import quadprog
#from cvxopt import matrix, solvers
from qpsolvers import solve_qp
import torch
#solvers.options['show_progress'] = False
#solvers.options['maxiters'] = 20
####### psi function #######
def ordered_BVN(P, C, TOLERANCE=10e-7):
    P2 = P.copy()
    C2 = C.copy()
    K = np.shape(C)[1]
    coefficients = []
    permutations = []
    c = 0  # sum of attributed weights
    i = 0
    while c < 1-TOLERANCE:
        i += 1
        if i > K**2:
            print("BVN", i, np.sum(P2))
        C2 = np.where(P2 > 0, C2, K)
        row_ind, col_ind = linear_sum_assignment(C2)
        coef = np.min(P2[row_ind, col_ind])
        permutations.append(col_ind)
        coefficients.append(coef)
        c += coef
        P2[row_ind, col_ind] -= coef
    return coefficients, permutations



######## phi function #######
def phi1(lbd, mu, N, P_prev):
    delta = delta_fun(lbd, mu)
    if not delta > 0:
        K = len(mu)
        return 1./K*np.ones((K,K))
    return ph1_aux(lbd, mu, N, delta, P_prev)



def objective(P,lbd,mu):
    N = np.shape(P)[0]
    l =-torch.log(torch.mv(P,mu)-lbd)
    l+=torch.linalg.norm(P)/(2*N)
    value = torch.max(l)
    return value

def delta_fun(lbd, mu):
    lbd2 = np.sort(lbd)[::-1]
    mu2 = np.sort(mu)[::-1]
    return np.min(np.cumsum(mu2[:len(lbd)]-lbd2)/np.arange(1, len(lbd)+1))

def ph1_aux(lbd, mu, N, delta, P_prev, niter=50,TOLERANCE=10e-4):
    P = P_prev
    lbd2 = torch.Tensor(lbd)
    mu2 = torch.Tensor(mu)
    Q, G, h, A, b = init_quadprog_variables(lbd, mu, N, delta)
    P2 = torch.autograd.Variable(torch.Tensor(P), requires_grad=True)
    for t in range(niter):
        obj = objective(P2, lbd2, mu2)
        if not torch.isnan(obj) and not torch.isinf(obj):
            obj.backward()
            P2.data -= 2*N/(t+1)*P2.grad.data
            P2.grad.data.zero_()
        # projection of P2
        q = torch.flatten(P2).detach().cpu().numpy().astype('double')
        proj = solve_qp(Q, q.reshape((N**2,)), G, h, A, b)
        proj = np.reshape(proj, (-1, N))
        P2.data = torch.Tensor(proj)
        temp = t/(t+2)*P+2/(t+2)*proj
        if np.linalg.norm(P-temp)<TOLERANCE:
            break
        P = t/(t+2)*P+2/(t+2)*proj
    return P


def random_bistochastic(N):
    a = np.random.rand(N**2)
    a = a/np.sum(a)
    P =np.zeros((N,N))
    for i in range(N**2):
        ind = np.random.permutation(range(N))
        D = np.zeros((N,N))
        D[range(N),ind] =1
        P+=a[i]*D
    return P

####### aux phi function for quadratic programming ######
def init_quadprog_variables(lbd,mu,N,delta):
    delta = delta_fun(lbd,mu)
    Q = np.identity(N**2)
    b = np.ones(N)
    A = np.zeros((N,N**2))
    for i in range(N):
        A[i,N*i:N*(i+1)] = 1
    G = np.zeros((2*N+N**2,N**2))
    d = np.array([i*N for i in range(N)])
    h = np.zeros(2*N+N**2)
    for i in range(N):
        G[i,N*i:N*(i+1)] = -mu
        h[i] = -(delta/np.sqrt(np.exp(1))+lbd[i])
        h[N+i] = 1
        G[N+i,d+i] = 1
        for j in range(N):
            G[2*N+N*i+j,N*i+j] = -1
    return Q,G,h.reshape((2*N+N**2,)),A,b.reshape((N,))
    #return matrix(Q),matrix(G),matrix(h),matrix(A),matrix(b)

def quadprog_solve_qp(Q, q, G=None, h=None, A=None, b=None):
    sol = solvers.qp(Q, q, G, h, A, b)
    return np.array(sol['x'])


# def test_in_domain(q,A,b,G,h):
#     T =1
#     temp = np.dot(A,q)-b.T
#     T = T*((temp==0).all())
#     temp = np.dot(G,q)-h.T
#     T = T*((temp<=0).all())
#     return T

from queues import Queue
import numpy as np
import matplotlib.pyplot as plt
import random 

class System:
	def __init__(self, queues, K, service_rates, rank=None):
		self.queues= queues
		self.K = K
		self.service_rates = service_rates
		self.packets = np.zeros(len(queues))
		self.choices = np.zeros(len(queues))
		self.queue_history = []
		self.server_history =[]


	def round(self, t):
		for q in self.queues:
			q.receive_packet(t)
		state = random.getstate()
		for i,q in enumerate(self.queues):
			self.packets[i], self.choices[i] = q.play(self.K,t)
			if self.queues[i].strategy=="collab":
				random.setstate(state)
		random.random()

		for k in range(self.K):
			if np.random.binomial(1,self.service_rates[k])==1:	
				pulling	= np.where(self.choices==k,self.packets,-1)
				oldest = np.max(pulling)
				if oldest>-1:
					winners = np.where(pulling==oldest)[0]
					winner = np.random.choice(winners)
					self.queues[winner].cleared()
				
		for i in range(len(self.queues)):
			if self.packets[i]>-1:
				self.queues[i].update_state()


	def state_summary(self,t,interval):
		queue_length = []
		servers_prop =[]
		for q in self.queues:
			queue_length.append(len(q.packets))
			servers_prop.append(q.server_history/interval)
			q.server_history = np.zeros(self.K)
		self.queue_history.append(queue_length)
		self.server_history.append(servers_prop)

	def save(self,filename):
		np.savetxt('data.csv', self.queue_history, delimiter=',')

		


				


